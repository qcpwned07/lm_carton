const versionLib = "Version script 1.2"

/* --- CONSTANTS --- */

// Tooltip
const tooltip = document.getElementById('tooltip');
const ttUnit = document.getElementById('unit');
const ttLimit = document.getElementById('limit');
// Alert
const theAlert = document.getElementById('alert');
const alertStrong = document.getElementById('alertStrong');
const alertText = document.getElementById('alertText');
// Other
const hideEmergencyButton = false;
const charToAvoid = ['&','~','\"','#','\'','{','(','[','-','|','`','è','é','_','\\','ç','^','à','@',')',']','°','=','+','}','¨','£','$','¤','*','µ','%','ù','!','§',':','/','.',';',',','?','<','>',' '];

/* --- GLOBAL VARIABLES --- */

// Reglages
var isReglageOpen = false;
var isLogged = false;
var action;
var	relay_act;
var isLocked = false; /* Buttons are locked: no heartbeat check */

// Page lock
var pageChangeLocked = true;
var isMenuLocked = false;

// Heartbeat
var watchDogInterval;
let watchdogDisabled = true;
const watchdogValue = 5000;

// Location
var url = "";
var module = false;

// Websockets
var websocket;
var ws;
let webSockets = [];
let webSocketsDict = {};

// Groups and Modules
let modulesDict = {};
let groupsDict = {};

/* DYNAMICS METHODS */
/* The tooltip follows the cursor */
function trackMouse(event){
    tooltip.style.top = event.pageY  + -50 + 'px';
    tooltip.style.left = event.pageX  + -245 + 'px';
}

/* Show the tooltip with custom values */
function displayTooltip(id){
    ttUnit.innerText = '';
    ttLimit.innerText = '';
    
    switch (id) {
        case 1:
            ttUnit.innerText = 'pouces';
            ttLimit.innerText = '0 à 1000';
		break;
		
        case 2:
            ttUnit.innerText = 'millisecondes';
            ttLimit.innerText = '1 à 100';
        break;
		
        case 3:
            ttUnit.innerText = 'impulsions par secondes';
            ttLimit.innerText = '100 à 2000';
        break;
		
        case 4:
            ttUnit.innerText = 'impulsions';
            ttLimit.innerText = '1 à 1000';
        break;
		
        case 5:
            ttUnit.innerText = 'pouce';
            ttLimit.innerText = '0,01 à 0,2';
        break;
		
        case 6:
            ttUnit.innerText = 'impulsions par centième de pouce';
            ttLimit.innerText = '1 à 10';
        break;
		
        case 7:
            ttUnit.innerText = 'pouces';
            ttLimit.innerText = '0 à 1000';
        break;
		
        case 8:
            ttUnit.innerText = 'Est-ce un axe circulaire ?';
            ttLimit.innerText = '0 à 1000';
        break;
		
        case 9:
            ttUnit.innerText = 'millisecondes';
            ttLimit.innerText = '0 à 300 000';
        break;
		
        case 10:
            ttUnit.innerText = 'pouces';
            ttLimit.innerText = '0 à 1000';
        break;
		
        case 11:
            ttUnit.innerText = 'pouce';	
			ttLimit.innerText = '0 à 1000';	
		break;
		
		case 12:		
			ttUnit.innerText = 'impulsion';	
			ttLimit.innerText = '';	
		break;
		
		case 13:	
            ttUnit.innerText = 'millisecondes';
            ttLimit.innerText = '';
        break;
                                                            
        default:
            break;
    }
    tooltip.style.display = "block";
    tooltip.style.position = "fixed";
    
}
/* Hide the tooltip */
function hideTooltip(){
    tooltip.style.display = "none";
}
/* Show the alert on user action */
function showAlert(type, text){
	theAlert.classList.remove('warn', 'success', 'danger');
	switch(type){
		case 'warn':
			alertStrong.innerText = "Chargement en cours...";
			break;
		case 'success':
			alertStrong.innerText = "Opération réussie !";
			break;
		case 'danger':
			alertStrong.innerText = "Erreur !";
			break;
		case 'info':
			alertStrong.innerText = "Info :";
			break;
	}
	//showSnackbar();
	theAlert.classList.add(type);
	alertText.innerText = text;
	theAlert.style.visibility = 'visible';
	if (type === "danger")
		setTimeout(hideAlert, 20000);
    else
		setTimeout(hideAlert, 2000);
}

function hideAlert(){
	theAlert.style.visibility = 'hidden';
}

// Validate (or not) the user's input when he sends it
function verifyInput(type, data){
	switch(data) {
		case 'moduleName':
			/* 20 char max, pas d'espaces, pas de ponctuation */
			if(data.length < 20) {
				return false;
			}
			charToAvoid.forEach(char => {
				if(data.includes(char)) {
					return false;
				}
			});
			break;
		case 'ssid':
			/* 15char max, pas d'espaces, pas de ponctuation */
			if(data.length < 15) {
				return false;
			}
			charToAvoid.forEach(char => {
				if(data.includes(char)) {
					return false;
				}
			});
			break;
		case 'password':
			/* 15char max */
			if(data.length < 15) {
				return false;
			}
			break;
		case 'nodeAddress':
			/* int de 1 à 99 */
			if(typeof data === 'number' && !isNaN(data)) {
				// check if it is integer
				if(!Number.isInteger(data) || data < 1 || data > 99) {
					return false;
				}
			} else {
				return false;
			}
			break;
		case 'actualPosition':
			/* int de 0 à 1000 */
			if(typeof data === 'number' && !isNaN(data)) {
				// check if it is integer
				if(!Number.isInteger(data) || data < 0 || data > 1000) {
					return false;
				}
			} else {
				return false;
			}
			break;
		case 'timeToStart':
			/* int de 1 à 100 */
			if(typeof data === 'number' && !isNaN(data)) {
				// check if it is integer
				if(!Number.isInteger(data) || data < 0 || data > 100) {
					return false;
				}
			} else {
				return false;
			}
			break;
		case 'motorSpeed':
			/* int de 100 à 2000 */
			if(typeof data === 'number' && !isNaN(data)) {
				// check if it is integer
				if(!Number.isInteger(data) || data < 100 || data > 2000) {
					return false;
				}
			} else {
				return false;
			}
			break;
		case 'nbPulsesToStop':
			/* int de 1 à 1000 */
			if(typeof data === 'number' && !isNaN(data)) {
				// check if it is integer
				if(!Number.isInteger(data) || data < 1 || data > 1000) {
					return false;
				}
			} else {
				return false;
			}
			break;
		case 'tolerance':
			/* float de 0.01 à 0.2 */
			if(typeof data === 'number' && !isNaN(data)) {
				// check if it is integer
				if(Number.isInteger(data) || data < 0.01 || data > 0.2) {
					return false;
				}
			} else {
				return false;
			}
			break;
		case 'scaleFactor':
			/* float > 0.1 */
			if(typeof data === 'number' && !isNaN(data)) {
				// check if it is integer
				if(data < 0.1) {
					return false;
				}
			} else {
				return false;
			}
			break;
		case 'maxRange':
			/* float de 0 à 1000 */
			if(typeof data === 'number' && !isNaN(data)) {
				// check if it is integer
				if(data < 0 || data > 1000) {
					return false;
				}
			} else {
				return false;
			}
			break;

		case 'parkPos':
			/* float de 0 à 1000 */
			if(typeof data === 'number' && !isNaN(data)) {
				// check if it is integer
				if(data < 0 || data > 1000) {
					return false;
				}
			} else {
				return false;
			}
			break;

		case 'runTime':
			/* int de 1 à 100k */
			if(typeof data === 'number' && !isNaN(data)) {
				// check if it is integer
				if(Number.isInteger(data) || data < 1 || data > 100000) {
					return false;
				}
			} else {
				return false;
			}
			break;
		case 'goToPos':
			/* int de 1 à 1000 */
			if(typeof data === 'number' && !isNaN(data)) {
				// check if it is integer
				if(Number.isInteger(data) || data < 1 || data > 1000) {
					return false;
				}
			} else {
				return false;
			}
			break;
		case 'ipAdress':
			const regex = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/gi;
			return regex.test(data);
	}
}


function clearAllRelays(){
	let marcheRelay = document.getElementById('marcheRelay');
	if (marcheRelay.checked){
		marcheRelay.checked = false;
		runRelay('marcheRelay');
	}

	let sensRelay = document.getElementById('sensRelay');
	if (sensRelay.checked){
		sensRelay.checked = false;
		runRelay('sensRelay');
	}
}

/* Handler pour la boite modale du mot de passe */
function passwordHandler(event){
	// Number 13 is the "Enter" key on the keyboard
  	if (event.keyCode === 13) {
    // Cancel the default action, if needed
    	event.preventDefault();
    // Trigger the button element with a click
    	document.getElementById('check').click();
  	}
}

function showModal(_action){
	action = _action;
	let modal = document.getElementById('myModal');
	modal.style.display = "block";
	let input = document.getElementById('password_reglages');
	input.focus();
	input.addEventListener("keyup", passwordHandler);
}
function hideModal(){
	let modal = document.getElementById('myModal');
	modal.style.display = "none";
}


function createSelectOption(name, value, ) {
	// Create the radio field
	let option =document.createElement("option");
	option.setAttribute("value", value);
	option.setAttribute("label", name.charAt(0).toUpperCase() + name.slice(1));
	return option
}

function showAddressModal(_action){
	action = _action;
	// Show the modal
	let modal = document.getElementById('addressModal');
	modal.style.display = "block";
	let select = document.getElementById("addressSelect");
	//document.getElementById("oldModuleName").innerText = module.name;
	//document.getElementById("hostname").innerText = module.name + ".local";
	// Iterate through all modules
	for (let group of groups){
		let gr = document.createElement("optgroup");
		gr.label = group.name;
		for (let module of group.modules){
			//var option = createSelectOption(module.name, module.url.split('.')[0]);
			var option = createSelectOption(module.name, module.url);  // THIERRY
			try{
				gr.appendChild(option);
			}
			catch (ex){
				console.log(ex);
			}
		}
		select.appendChild(gr)
	}
}

function hideAddressModal(){
	let modal = document.getElementById('addressModal');
	if (modal)
		modal.style.display = "none";
}

function changeModuleUrl(name){
	// demande au module de changer d'ip ou de mdns host name
	// le module peut ce déconnecter...
	if (!isLogged){
		showModal('changeModuleUrl');
	}
	if (isLogged){
		console.log("changeModuleUrl, new url: " + name + " will clear watchdog");
		showAlert("warn", "Changement de l'url du module...");
		disableWatchDog();
		try {
			websocket.send(JSON.stringify({"moduleUrl":name}));
		}
		catch(ex){
			console.log(ex);
			showAlert('danger', "Une erreur est survenue, merci de réessayer plus tard.");
		}
	}
}

function saveNodeAddress(node){
	console.log("Changing node address (before)");
	if (!isLogged){
		showModal('saveNodeAddress');
	}
	
	if (isLogged){
		showAlert('warn', "Changement de l'adresse RS422");
		try {
			websocket.send(JSON.stringify({"nodeAddress":node}));
		}
		catch (ex) {
			showAlert("danger", "Une erreur est survenue, merci de réessayer plus tard.");
		}
	}
}

function saveMechanicals(obj = null){
	console.log("Saving mechanicals");
	let currentModule = module;
	// Test if logged
	if (!isLogged){
		showModal('saveMechanicals');
	}
	if (isLogged){
		showAlert('warn', "Changement des données mécaniques");
		var mechanicals;
		// If the MECHANICALS object is not provided
		if (obj == null) {
			// Check if device rotatif, with existence verification
			let rep;
			let isDeviceRotatif = document.getElementById('isDeviceRotatif');
			if (isDeviceRotatif === undefined || isDeviceRotatif === null)
				isDeviceRotatif = {checked: false};
			(isDeviceRotatif.checked === true ? rep = 'checked' : rep = 'unchecked');
			// Check if mechanicals locked, with existence verification
			var pep;
			let isMechanicalsLocked = document.getElementById('lockMotorParameters');
			if (isMechanicalsLocked === undefined || isMechanicalsLocked === null)
				isMechanicalsLocked = {checked: false};
			(isMechanicalsLocked.checked === true ? pep = 'checked' : pep = 'unchecked');
			// Create the JSON MECHANICALS object
			mechanicals = JSON.stringify(
				{
					"scaleFactor": document.getElementById("scaleFactor").value,
					"actualPosition": document.getElementById("actualPosition").value,
					"timeToStart": document.getElementById("timeToStart").value,
					"nbPulsesToStop": document.getElementById("nbPulsesToStop").value,
					"motorSpeed": document.getElementById("motorSpeed").value,
					"tolerance": document.getElementById("tolerance").value,
					"maxRange": document.getElementById("maxRange").value,
					"parkPos": document.getElementById("parkPos").value,
					"isDeviceRotatif": rep,
					"isMechanicalsLocked": pep
				});
		}
		else {
		    // Faire le rapport du scale factor pour modifier la position actuelle
            let actual = document.getElementById("actualPosition").value;
            let scalePrev = module.mechanicals.scaleFactor;
			let scaleNext = obj.scaleFactor;
			// Scale position
            console.log("module.scaleFactor", scalePrev);
			console.log("obj.scaleFactor", scaleNext);
			console.log("actual", actual);
			console.log("after", (scaleNext / scalePrev) * actual);
			console.log("OBJ AVANT");
			console.log(obj);
			obj.actualPosition = (scaleNext / scalePrev) * actual;
			console.log("OBJ APRES");
			console.log(obj);
			mechanicals = JSON.stringify(obj);
		}

		console.log("Sending mechanicals: ", mechanicals);
		try{
			websocket.send(mechanicals);
			//setTimeout(function(){websocket.send(JSON.stringify({"url":"reglages.html"}))}, 1000)
			websocket.send(JSON.stringify({"url":"reglages.html"}));
		}
		catch (ex) {
			showAlert('danger', 'Une erreur est survenue, merci de réessayer plus tard.');
		}
	}
}

function runForA_GivenTime(){
	if (!isLogged){
		showModal('runForA_GivenTime');
	}
	
	if (isLogged){
		showScaleFactorTool("false");
		console.log("Bouton runForA_GivenTime");
		const sensDep = document.getElementById('sensDep');
		var rep;
		(sensDep.checked ? rep = 'checked' : rep = 'unchecked');
		try {
			websocket.send(JSON.stringify({"time":document.getElementById('runTime').value, "sensDep":rep}));
			showAlert("warn", "L'ordre de déplacement pour un temps donné a été envoyé.");
		} 
		catch (ex) {
			showAlert("danger", "Une erreur est survenue, merci de réessayer plus tard.");
		}
	}
}

function moveWith(dep){
	console.log("Bouton déplacement relatif");
	try {
		websocket.send(JSON.stringify({"relativeMove":dep.toString()}));
		showAlert("warn", "L'ordre de déplacement relatif a été envoyé."); 
	}
	catch (ex) {
		showAlert("danger", 'Une erreur est survenue, merci de réessayer plus tard.');
	}
}

function checkPassword(){
	websocket.send(JSON.stringify({"password_reglages":document.getElementById('password_reglages').value}));
}

function runRelay(relay){
	if (!isLogged){
		showModal('runRelay');
		relay_act = relay;
	}
	
	if (isLogged){
		theRelay = document.getElementById(relay);
		var rep;
		(theRelay.checked ? rep = 'checked' : rep = 'unchecked');
		try {
			if(relay === 'marcheRelay')
				websocket.send(JSON.stringify({marcheRelay:rep}));
			if(relay === 'sensRelay')
				websocket.send(JSON.stringify({sensRelay:rep}));
			if(relay === 'ledRelay')
				websocket.send(JSON.stringify({ledRelay:rep}));
		}
		catch(ex) {
			console.error("Caramba !", ex.message);
		}
		 showAlert('success', 'Le relais a été commandé: ' + (theRelay.checked==true ? "marche": "arrêt"));
	}
}

function goToPos(){	
	let fieldGTP = document.getElementById('goToPos');
	let btnGTP = document.getElementById('goToPosBtn');
	if(fieldGTP.value === '' || fieldGTP.value === ' '){
		showAlert('danger', "Attention une valeur doit être saisie dans le champ 'Aller à la position'.");
	} else {
		try{
		websocket.send(JSON.stringify({"newPos":document.getElementById('goToPos').value}));
	} 
	catch(ex) {
		 showAlert('danger', "Exception pendant l'envois de la commande 'goToPos()'.");
	}
	showAlert('warn', "L'ordre d'aller a une position absolue a été envoyé.");
	}
	
}

function changeWifiParameters(){
	if (!isLogged){
		showModal('changeWifiParameters');
	}
	
	if (isLogged){
		showAlert('warn',"Attention, le module va changer de paramètres wifi!");
			
		websocket.send(JSON.stringify(
			{
				"ssid":document.getElementById("ssid").value,
				"password":document.getElementById("password").value
			}
		));	
	}
}

function onLoadTooMuchClients(){
	var moduleName = location.search.split('moduleName=')[1];
	console.log("Location has changed, moduleName is: " + moduleName);
	document.title=moduleName;
	document.getElementById("moduleName").value = moduleName;
} 

function onLoad(){
	//initWebSocket();
	isLogged = false;
	/* Deactivate emergency button*/
	if (hideEmergencyButton == true){
		document.getElementById("emergencyDiv").style.display = "none";
	}
}

function onOpen(event) {
	let pathArray = window.location.pathname.split('/');
	if (pathArray[1] == ""){
		pathArray[1] = "index.html";
	}
	console.log("onOpen: pathArray: " + pathArray[1] + "readyState: " + websocket.readyState);
	if (websocket.readyState == 1){
		try {
			websocket.send(JSON.stringify({"url": pathArray[1]}));
			registerWebsocket(websocket, url);
			showAlert('success', "Connecté au serveur en attente des paramètres");
			console.log("onOpen: Connecté au serveur en attente des paramètres");
			enablePanel();
			btnStatusChange(true);
		} catch (e){
			disableInterface(module.url);
			console.log("onOpen error: " + e.message);
		}
	} else {
		console.log("onOpen: webSocket en cours de connection, readyState: " + websocket.readyState);
	}
}

function tryReconnect(){
    console.print("In tryReconnect() from watchDog |  URL:" + url);
	console.log("In tryReconnect() from Watchdog|  URL:" + url);
	initWebSocket();
}

function disableWatchDog(){
	console.log("Watchdog is disabled");
	console.log("watchdogInterval: " + watchDogInterval + " before");
	clearInterval(watchDogInterval);
	watchdogDisabled = true;
}

function enableWatchDog(){
	console.log("Watchdog is enabled");
	console.log("watchdogInterval: " + watchDogInterval + " before");
	clearInterval(watchDogInterval);
	watchDogInterval = setInterval(watchDog, watchdogValue);
	console.log("watchdogInterval: " + watchDogInterval + " after");
	watchdogDisabled = false;
}

function watchDogRearm(){
	if (!watchdogDisabled){
		console.log(watchDogInterval + " before");
		clearInterval(watchDogInterval);
		watchDogInterval = setInterval(watchDog, watchdogValue); 
		console.log("watchdogInterval: " + watchDogInterval + " after");
		console.log("Watchdog is rearmed: " + watchdogValue);
	} else {
		console.log("try to rearm watchdog but watchdog is disabled !");
	}
}

function watchDog(){
	/* Le watch dog est de 10 minutes pendant le déplacement */
	// Prepare for reactivation
    closeReglages();
    console.print("In WATCHDOG | url:" + url);
	console.log("In WATCHDOG | url:" + url + " watchDogInterval: ");
	showAlert('warn', " WatchDog!\n Fermeture du websocket...");
	
	try{
		console.print("Watch dog closing connection" + Date.now());
		websocket.close();
		cleanWebSockets();
	} catch (e){
		console.error("Error while closing websocket" + e);
	}
	setTimeout(tryReconnect, 500); /* il faut laisser le temps au serveur de supprimer l'ancien client */
}

function scaleFactorTool(){
	let pos = document.getElementById("rightPos").value;
	websocket.send(JSON.stringify({"scaleFactorTool":pos}));
	showAlert('warn', "La position réel de l'axe a été envoyée au serveur");
}

function showScaleFactorTool(state){
	if (isReglageOpen){
		sc = document.getElementById('sc');
		if (state=="true"){
			sc.style.display='block';
		}else {
			sc.style.display='none';
		}
	}
}

function btnStatusChange(lock){
	//var navBtn = document.getElementById('navigationBtn');
	console.log("btnStatusChange, lock: " + (lock == true ? "true" : "false"));
	let elmsToDisable = document.getElementsByClassName("toDisable");
	if (lock){
		//clearInterval(watchDogInterval);
		//watchDogInterval = setInterval(watchDog, 10 * 60 * 1000); /* 10 minutes pendant le mouve*/
		//navBtn.classList.remove('green');
		//navBtn.classList.add('navDisabled');
		//navBtn.disabled = true;
		document.body.style.cursor = "not-allowed";
		for(let i = 0; i < elmsToDisable.length; i++) {
			elem = elmsToDisable[i];
			elem.disabled = true;
			if(elem.tagName === 'INPUT') {
				elem.classList.add('inputsDisabled');
			} else if(elem.tagName === 'BUTTON') {
				/*console.log(elem.id, elem.classList);*/
				elem.classList.remove('blue');
				elem.classList.add('btnsDisabled');
			}
		}
		if (isReglageOpen){
			document.getElementById('marcheRelay').classList.remove('inputsDisabled');
			document.getElementById('marcheRelay').disabled=false;
			document.getElementById('btn_marcheRelay').classList.remove('btnsDisabled');
			document.getElementById('btn_marcheRelay').classList.add('blue');
			document.getElementById('btn_marcheRelay').disabled=false;
			
		}
		pageChangeLocked = true;
	} else {
		/* on reactive tout */
		//navBtn.classList.add('green');  /* reactive le bouton de navigation entre page */
		//navBtn.classList.remove('navDisabled');
		//navBtn.disabled = false;
		document.body.style.cursor = "auto";
		isMenuLocked = false;

		for(let i = 0; i < elmsToDisable.length; i++) {
			elem = elmsToDisable[i];
			elem.disabled = false;
			if(elem.tagName === 'INPUT') {
				elem.classList.remove('inputsDisabled');
			} else if(elem.tagName === 'BUTTON') {
						elem.classList.remove('btnsDisabled');
						elem.classList.add('blue');
			}
		}
		pageChangeLocked = false;
	}
	if (isLocked != lock){
		console.log("isLocked: "  + (lock == true ? "true" : "false"));
		if (lock) 	console.log("La navigation va être désactivée et tous les boutons vont être bloqués");
		else 		console.log("La navigation va être rétablie et les boutons réactivés");
	}
	sLocked = lock;
}

function onClose(event) {
	if (event.target.url === window.url)
	console.log('Connection closed', event);
}

function updatePos(obj){
    if (!obj)
    	console.error("updatePos(obj) was called with  obj = null\n obj: " + obj);
	else if (!obj["actualPosition"])
		console.error("updatePos(obj): obj has no attribute actual position \nobj:" + obj);
	
	if (isReglageOpen){
		setTimeout(function(){
			let $actualPosition = document.getElementById("actualPosition");
			if ($actualPosition){
				$actualPosition.value = obj["actualPosition"];
			}
		}, 100);
	} else {
			let $goToPos = document.getElementById("goToPos");
			let $actualPosition = document.getElementById("actualPosition");
			if ($goToPos)
				$goToPos.value = obj["actualPosition"]; /* index.html */
			else if ($actualPosition)
				$actualPosition.value = obj["actualPosition"];
	}
}

function updatePosRun(pos){
    // Mise a jour de la position pendant le mouvement
	if (isReglageOpen){
				document.getElementById("actualPosition").value = pos; /* reglage.html */
	} else {
				document.getElementById("goToPos").value = pos; /* index.html */
	}	
}

function back(){
	websocket.send(JSON.stringify({"back":"true"}));
	showAlert('warn', "Retour à la position.");	
}

function goToPark(){
	websocket.send(JSON.stringify({"goToPark":"true"}));
	btnStatusChange(true)
	showAlert('info', "L'axe se déplace en position de park.");
}

function updateMove(obj){
	let settings = document.getElementById("settings");
	if (isReglageOpen){
		document.getElementById("moveInpuls").value = obj["movePulses"];					
		document.getElementById("moveInches").value = obj["move"];
		document.getElementById("timeToStart").value = obj.timeToStart;
		document.getElementById("nbPulsesToStop").value = obj["nbPulsesToStop"];	
		document.getElementById("motorSpeed").value =  obj["motorSpeed"];
		document.getElementById("actualPosition").value = obj.actualPosition;
	} 
	
	if (obj.hasOwnProperty("axeblocked")){
			if (obj["axeblocked"] == "true"){
				document.getElementById("axeblockedDiv").style.visibility = "visible";
			} else {
				document.getElementById("axeblockedDiv").style.visibility = "hidden";
			}
	}
		
	document.getElementById("goToPos").value = obj["actualPosition"]; /* index.html */

	let success = obj.isdisplacementOK;
	showAlert(success == 'true' ? 'success': 'danger', success === 'true' ? "L'axe est à la position prévue": "l'axe est en dehors de la tolérance");
	if (!success && obj["movePulses"] == 0){
		showAlert("danger", "L'axe n'a vu aucun déplacement, cela fait pensser à un problème dans la chaine de l'encodeur (connecteur, etc.)");
	}
}

function deviceRotatif(isDeviceRotatif, maxRange, parkPos){
	if (isDeviceRotatif == 'true'){
		if (isReglageOpen){
			document.getElementById("maxRangeSpan").style.display = "block";
			document.getElementById("maxRange").value = maxRange;
			
			document.getElementById("ParkPosSpan").style.display = "block";
			document.getElementById("goToparkPos").style.display = "block";
			document.getElementById("goToparkPosMain").style.display = "block";
			document.getElementById("parkPos").value  = parkPos;
		} else {
			document.getElementById("goToparkPosMain").style.display = "block";
		}
	} else {
		if (isReglageOpen){
			document.getElementById("maxRangeSpan").style.display = "none";
			
			document.getElementById("ParkPosSpan").style.display = "none";
			document.getElementById("goToparkPos").style.display = "none";
			document.getElementById("goToparkPosMain").style.display = "none";
			document.getElementById("parkPos").value  = parkPos;
		} else {
			document.getElementById("goToparkPosMain").style.display = "none";
		}
	}
	if (isReglageOpen){
		document.getElementById("isDeviceRotatif").checked = (isDeviceRotatif === "true" ? true : false);
		
	}
}

function tooMuchClients(moduleName){
	console.log("url will be: " + 'tooMuchClients.html' + '?moduleName=' + moduleName);
	window.location.href ='tooMuchClients.html' + '?moduleName=' + moduleName;
}

function scaleFactorUpdate(obj){
	document.getElementById("actualPosition").value = obj.actualPosition;
	
	if (isReglageOpen){
		document.getElementById("scaleFactor").value = obj.scaleFactor;		
	}
	
	//btnStatusChange(false);	
	showAlert("success", "Le scale factor et la position actuelle ont été mis à jour.");
}

function updateModuleName(obj){
	document.getElementsByClassName("moduleName")[0].value = obj.moduleName;
	/*document.getElementById("moduleName").style.backgroundColor =  "lightblue";  A modifier pas beau ! */
	document.title=obj.moduleName;
	deviceRotatif(obj.isDeviceRotatif, obj.maxRange, obj.parkPos);

	if (isReglageOpen){
		document.getElementById("nodeAddress").value = obj.nodeAddress;
		document.getElementById("actualPosition").value = obj.actualPosition;
		document.getElementById("ssid").value = obj.ssid;
		document.getElementById("password").value = obj.password;
		document.getElementById("scaleFactor").value = obj.scaleFactor;
		document.getElementById("timeToStart").value = obj.timeToStart;
		document.getElementById("nbPulsesToStop").value = obj.nbPulsesToStop;
		document.getElementById("motorSpeed").value = obj.motorSpeed;
		document.getElementById("tolerance").value = obj.tolerance;
		document.getElementById("lockMotorParameters").checked = (obj.isMechanicalsLocked == "true" ? true : false);
		document.getElementById("lockMotorParameters").checked = (obj.isMechanicalsLocked == "true" ? true : false);
		
		document.getElementById('ip').value = obj.Ip;
	} else {
		document.getElementById("goToPos").value = obj.actualPosition;
	}
	enablePanel()
}

function hideButtons(status){
	console.log( "hideButtons: status: " + status);
	if (status=="true"){
		// on va bloquer tout
		btnStatusChange(true);  // cacher les boutons
		isMenuLocked = true;
	} else {
		enableInterface(module.url);			// montrer les boutons
	}
	websocket.send(JSON.stringify({"handshake":"buttons"}));
}

function onMessage(event){
	// Remove panel overlay
	enablePanel();
    // 0. Show module response
	console.log(event.data);
	// 1. Take appropriate action
	try{
		var obj = JSON.parse(event.data);
		if (obj.hasOwnProperty("actualPosition")){
			updatePos(obj);
		}
		console.log("Message from web socket:\n", obj);
		watchDogRearm();
		switch(true){
			case obj.hasOwnProperty("tooMuchClients"):
				tooMuchClients(obj.moduleName);
				break;

			case obj.hasOwnProperty("movePulses"):
				updateMove(obj);
				break;

			case obj.hasOwnProperty("hideButtons"):
				hideButtons(obj.hideButtons);
				break;

			case obj.hasOwnProperty("moduleName"):
				updateModuleName(obj);
			break;

			case obj.hasOwnProperty("handshake"):
				var t = obj.handshake;
				if(t.includes("wifi")){
					showAlert('success', "Les paramètres wifi ont été enregistrés avec succes");
				}

				if (t.includes("nodeAddress")){
					showAlert('success', "L'adresse RS422 du module a bien été changée.");
				}

				if (t.includes("mechanicals")){
				   showAlert('success', "Paramètres mécaniques enregistrés avec succés sur le module");
				   deviceRotatif(document.getElementById("isDeviceRotatif").checked == true ? "true" : "false", document.getElementById("maxRange").value, document.getElementById("parkPos").value);
				}

				if (t.includes("mechanicalsFault")){
					showAlert('danger', "Il y a une erreur dans le fichier index.html pour ce module !!!!!");
				}

				if (t.includes("moduleName")){
				   let u = "http://" + document.getElementById("addressSelect").value + ".local";
				   showAlert('success', 'Après le reset du module, la nouvelle url sera: ' + u.toLowerCase());
				}

				if (t.includes("emergency")){
					marcheRelay = document.getElementById('marcheRelay');
					if (isReglageOpen){
						clearAllRelays();
					}
					console.log("Emergency quittance received");
					showAlert('danger', "La machine est en arrêt d'urgence!");
				}

			break;

			case obj.hasOwnProperty("scaleFactorUpdate"):
				scaleFactorUpdate(obj);
			break;

			case obj.hasOwnProperty('password_reglages'):
				var t = obj.password_reglages;
				if (t.includes("OK")){
					executeAction();
				} else {
						showAlert('warn', 'Le mot de passe saisi ne correspond pas.');
				}
			break;

			case obj.hasOwnProperty('heartbeat'):
				heartbeatReceive();
			break;

			case obj.hasOwnProperty('showScaleFactorTool'):
				showScaleFactorTool(obj.showScaleFactorTool);
			break;

			case obj.hasOwnProperty('actualPositionRun'):
				updatePosRun(obj.actualPositionRun);
			break;

			default: console.log(obj);
			break;
		}
	} 	catch (e) {
		console.log("Exception on parsing json message from serveur:\n " + event.data + "\n" + e);
	}
}

function heartbeatReceive(){
	console.log("heartbeat received");
	websocket.send(JSON.stringify({"heartbeat":"heartbeat"}));
	enablePanel();
}

function executeAction(){
	isLogged = true;	
	var modal = document.getElementById('myModal');
	modal.style.display = "none";
	
	showAlert('success', 'Vous êtes autentifé avec succès.');
	switch (action){
		case 'runRelay': runRelay(relay_act);
		break;
		/*
		case 'changeModuleName': changeModuleName();
		break;
		*/
		case 'changeWifiParameters': changeWifiParameters();
		break;
		
		/*
		case 'saveNodeAddress': saveNodeAddress();
		break;
		*/
		
		case 'saveMechanicals': saveMechanicals();
		break;
		
		case 'runForA_GivenTime': runForA_GivenTime();
		break;

		case 'updateAddress': updateAddress();
			break;

		default: console.log("Action non reconnue!");
				 showAlert("danger", "Action non reconnue: " + action);
		break;
	} 
	
}

function initWebSocket() {
	console.log("Init websocket url: " + url);
	console.print("initWebsocket() | URL:" + url);
	
	try {
		if (url !== ""){
			console.log("initWebSocket will create a web socket: " + url);
			websocket = new WebSocket("ws://" + url + "/ws");
		}
		else{
			console.error("Line 1554: unreachable code reached")
			websocket = new WebSocket("ws://" + window.location.hostname + "/ws");
		}
		disableInterface(url);
		websocket.onopen = onOpen;
		websocket.onclose = onClose;
		websocket.onmessage = onMessage;
		websocket.onerror = function(event) {
								let reconnectButton = `<button onclick="()=>connectTo('${url}')" class='btn center reconnect'>Reconnection</button>`;
								disableInterface(url, reconnectButton);
								cleanWebSockets();
								console.error("WebSocket error observed:", event);
								showAlert("danger",
									"Erreur sur le websocket: " + url + "\n Tentative de reconnection...");
								

							};
	} catch (e) {
		console.log(e, e.data);
		showAlert("danger", "Erreur sur le websocket: " + e);
	}
}

function cleanWebSockets(){
	console.log("Clean web sockets: nb elements: " + webSockets.length);
	let idToRemove;
	let hasToRemove = false;
	console.log("websockets before cleaning: ", webSockets);
	for (let i in webSockets){
		if (webSockets[i].readyState === 3){
			console.log("Will remove id: ", i, "ws: ", webSockets[i]);
			idToRemove = i;
			hasToRemove = true;
		}
	}
	
	if (hasToRemove){
		webSockets.splice(idToRemove, 1);
	}
		
	console.log("websockets after cleaning: ", webSockets);
}

// ----------------------------- //
function registerWebsocket(ws, url) {
	// 0. Clean up the websocket dicts
	
	cleanWebSockets();
	
	// 1. Add ws to websockets globals
	webSocketsDict[url] = ws;
	webSockets.push(ws);
	console.log("Websockets after registration", webSockets);
	console.log("Websocket DICT", webSocketsDict)
	
	console.log("websockets after push: ", webSockets);
}


// ----------------------------- //
//   FCTS AJOUTES PAR MATTHIEU   //
// ----------------------------- //

/**
 * Init the global variable groupsDict and modulesDict
 */
function initGlobals(groups){
	for (let group of groups) {
	    groupsDict[`${group.name}`] = group
		for (let module of group.modules) {
			modulesDict[`${module.url}`] = module
		}
	}
}

/**
 * Fonction permettant de se connecter a un module ESP32 a l'adresse indique par url
 *
 * @param url <String> : URL du ESP32
 * @return websocket: La connexion active
 */
function connectTo(url) {
	window.url = url
	console.print("connectTo url: " + url);
	console.log("connectTo url: " + url);
	console.log("Will enable watchdog");
	enableWatchDog();

	let reconnectButton = `<button onclick="()=>connectTo('${url}')" class='btn center reconnect'>Reconnection</button>`;
	// If connexion is not already established
    if (websocket && websocket.OPEN)
    	websocket.close();

	// Connect through socket to URL
	try {
		websocket = new WebSocket("ws://" + url + "/ws");
		websocket.onopen = onOpen;
		websocket.onclose = onClose;
		websocket.onmessage = onMessage;
		websocket.onerror = function(event) {
			console.error("WebSocket error observed:", event);
			showAlert("danger", "Exception sur le websocket (onError): " + url);
			disableInterface(url, reconnectButton);
			cleanWebSockets();
			//initWebSocket();
		};
	} catch (e) {
		console.log(e);
		showAlert("danger", "Erreur lors de la connexion au module: " + url);
		disableInterface(url, "<p class='warn'>Erreur lors de la connection</p>");
	}
	showAlert("warn", "Tentative de connexion au module " + url);
	return websocket;
}



/**
 *  TODO - To be removed once tested while commented out
 * Fonction permettant de changer le websocket persistant global.
 *
 * Cette fonction doit etre appelee avant chaque commande qui ne vise pas le meme modules que la precedente.
 * @param url
function socketFocus(url) {
	websocket = webSocketsDict[url]
}
 */

function disconnect(url) {
	console.print("Déconnection en cours du socket");

	try{
		webSocketsDict[url].close();
	} catch {
		console.print("catch webSocketsDict")
	}
	
	try {
		websocket.close();
	} catch {
		console.print("catch webSocket")
	}
	
	cleanWebSockets();
	disableWatchDog();
	disableInterface(url)
}

/**
 * Fonction qui bloque toute les commandes d'un module
 *
 * @param url: url du module a desactiver
 * @param child: Composante a afficher a l'interieur du overlay
 */
function disableInterface(url, child=null) {
	let el = document.getElementsByClassName("overlay")[0] ;
	if (el){
		el.style.display = 'flex';
		if (child)
			el.innerHTML = child;
	}
	else (console.log("Trying to disable interface before the html is loaded"))
}

/**
 * Fonction qui bloque toute les commandes d'un module
 *
 * @param url: url du module a desactiver
 */
function enableInterface(url) {
	let overlay = document.getElementsByClassName("overlay")[0];
	// Protect if the interface is not already loaded
	if (overlay)
		overlay.style.display = 'None';
    btnStatusChange(false);  // montrer les boutons
}

/**
 * Enables the panel (without enabling the buttons)
 */
function enablePanel() {
	let overlay = document.getElementsByClassName("overlay")[0];
	// DO only if is already loaded
	if (overlay)
		overlay.style.display = 'None';
}

/**
 * Builds the top level menu and adds it to the page (HTML)
 *
 * @param groups [{group.name, group.modules[] }]
 */
function menuBuilder(groups){
	// Save the groups within a global dict
	// Select the nav menu and create the links
	let nav = document.getElementById("nav");
	let links = "";
	for (let group of groups){
		groupsDict[group.name] = group;
		groupString = JSON.stringify(group);
		links += `<a id="${'nav_' + group.name}" onclick=submenuBuilder("${group.name}") >${group.name.toUpperCase()}</a>`;
		nav.innerHTML = links;
	}
	submenuBuilder(groups[0].name);
	console.log(groupsDict);
}

/**
 * Builds the submenu for the group currently selected and display it in the HTML
 *
 * @param groupName
 */
function submenuBuilder(groupName){
	group = groupsDict[`${groupName}`];
	let subnav = document.getElementById("subnav");
	let links = "";
	for (let module of group.modules)
		links += `<a id="${'nav_' + module.url}" onclick=setModule("${module.url}")>${module.name.toUpperCase()}</a>`;
	// Set the subnav links and the new page title
	subnav.innerHTML = links;
	// Remove active link
	prevActive = document.getElementById("nav").getElementsByClassName("active")[0]
	if (prevActive)
		prevActive.classList.remove("active");
	// Set new active
	document.getElementById("nav_"+ groupName).setAttribute("class", "active");
}

/**
 * Fonction qui change le module courant
 *
 * Établi une connexion, load l'interface graphique, et change les variable globale pour refléter le changement d'état
 */
function setModule(moduleURL) {
	if (isReglageOpen){
		closeReglages();
		hideAddressModal()
	}
	try {
		disconnect(url)
	} catch (e) {
		console.print("setModule()");
		console.print("Error" + e)
	}
    // Get module and set it to global
	module = modulesDict[moduleURL];
	// Change global
	url = moduleURL;
	window.url = moduleURL;
	// Select content div
	let el = document.getElementById("components");
	// Connect to module
	console.print("SetModule: " + module.name + " url: " + url);
	console.log("SetModule: " + module.name + " url: " + url);
	ws = connectTo(moduleURL);
	// Set group and module active in the menu
	submenuBuilder(module.group);
	// Remove previous active tag
	prevActive = document.getElementById("subnav").getElementsByClassName("active")[0]
	if (prevActive)
		prevActive.classList.remove("active");
	// Add the active on the navigation selected module
	document.getElementById("groupName").innerText = module.group
	while (el.lastChild)
		el.removeChild(el.lastChild);
	controlPanelFactory(module);
}

/**
 * Fonction qui retire tout les enfants de "components" et les remplace par un tableau de bord pour le module passe en param
 *
 * @param module: <String> Nom du module
 */
function controlPanelFactory(module) {
	let urlTemp = "./esp32.html";
	let url = module.url;
	let connectButton = `<button onclick="()=>connectTo('${url}')" class='btn center reconnect'>Reconnection</button>`;
	fetch(urlTemp)
		.then(response => response.text())
		.then(data => {
			let el = document.getElementById("components");
			let child = document.createElement("div");
			child.setAttribute("id", url)
			child.innerHTML = data;
			moduleName = child.getElementsByClassName("moduleName")[0].innerHTML = module.name.toUpperCase();
			disabledWrapper = document.createElement("div");
			disabledWrapper.innerHTML = "" +
				"<p style='color:green'> <span class='disabledText'>Trop de clients</span></p>";
			child.setAttribute("id", url);
			child.setAttribute("class", "controls");
			el.appendChild(child);
			disableInterface(url, connectButton);
		});
}


function loadReglages() {
	let localURL = './reglages.html';
	// Get the reglages page from server
	fetch(localURL)
		.then(response => response.text())
		.then(html => window.reglageHTML = html)
}
/**
 * Fonction qui ajoute les réglages à la page actuelle (DOM).
 * Les réglages sont inséré dans la div "components" comme dernier enfant
 */
function openReglages(){
    // Do nothing if the page is currently locked
	if (pageChangeLocked){
		console.log("Le changement de page est bloqué");
		showAlert("warn", "L'ouverture des réglages est présentement bloquée");
		return;
	}
	// Do nothing if settings are already open
	else if (isReglageOpen) {
		console.log("Le changement de page est bloqué");
		showAlert("warn", "Les réglages sont déjà ouverts");
		return;
	}
	let localURL = './reglages.html';
	// Get the reglages page from server
	// First get the page
	let el = document.getElementById("mySidenav");
	let child = document.createElement("div");
	child.setAttribute("id", "settings");
	child.innerHTML = window.reglageHTML;
	// Set the module name
	child.getElementsByClassName("moduleName")[0].innerHTML = module.name;
	// Open the drawer
	document.getElementById("mySidenav").style.width = "80%"; //opens side navbar by 70 percent
	document.getElementById('backdrop').style.display = "block"; //displays overlay
	el.appendChild(child);
	websocket.send(JSON.stringify({"url":"reglages.html"}));
	// Update global state
	isReglageOpen = true;
}

/**
 * Fonction qui retire l'interface de réglages du DOM
 */
function closeReglages() {
	// 0.Vérifier que les reglages sont bien ouvert
    if (isReglageOpen){
    	// 1.0 Assigner la nouvelle valeur de position au tableau de bord de l'index
        document.getElementById('goToPos').value = document.getElementById("actualPosition").value;
		let settingsDiv = document.getElementById("settings");
		let componentsDIv = document.getElementById("mySidenav");
		// 1.1 Fermer les réglages
		componentsDIv.removeChild(settingsDiv);
		isReglageOpen = false;
		document.getElementById("mySidenav").style.width = "0";
		document.getElementById('backdrop').style.display = "none";
		// Rediriger le socket sur index.html
		websocket.send(JSON.stringify({"url": "index.html"}));
	}
}

/**
 * Function that toogle isDeviceRotatif for this module
 */
function changeRotatif() {
    return 0;
}

/**
 *  Fonction qui met à jour le nom du module, l'adresse Rs422 et les paramètre mécaniques
 *
 *  Appellée lors de la soumission du formulaire dans le modal updateAddress
 *  dans les réglages
 */
function updateAddress(){
	let module;
	
	if (!isLogged){
		showModal('updateAddress');
	}
	if (isLogged){
		console.log(document.getElementById("addressSelect").value);
		module = modulesDict[document.getElementById("addressSelect").value];  // THIERRY
		console.log("updateAddress: module.url: " + module.url + "addressSelect: " + document.getElementById("addressSelect").value);
		saveNodeAddress(module.node);
		setTimeout(function(){saveMechanicals(module.mechanicals)}, 100);
		setTimeout(function(){changeModuleUrl(module.url)}, 1000);
		setTimeout(function(){setModule(module.url)}, 2000);
	}
}

/**
 * Function pinging the url with a simple get (to prevent ICMP firewall rejects)
 * For debug purposes
 *
 * @param url
 * @returns {string}
 */
function httpGET(url) {
	let xmlHttpReq = new XMLHttpRequest();
	xmlHttpReq.open("GET", "http://"+url, false);
	xmlHttpReq.send(null);
	return xmlHttpReq.responseText;

}


/**
 * Send emergency stop to the url
 * @param url: The url to send the signal on
 * @returns {WebSocket} The socket sending the signal onOpen
 *content.html
 **/
function stopWS(url){
	let ws;
	try {
		ws = new WebSocket("ws://" + url + "/ws");
		ws.onopen = ()=>{sendStop(ws)};
		ws.onmessage = ()=>{ws.close()};
		ws.onclose = ()=>{console.log("Socket for ", url, " is close")};
		ws.onerror = function(event) {
				console.error("WebSocket error observed while trying to stop:", event); };
	}
	catch { console.log("Error while trying to send emergency stop to ", url) }
	return ws
}

/**
 * Send an emergency stop message on the given socket
 * @param ws - Socket of the module to stop
 */
function sendStop(ws) {
	ws.send(JSON.stringify({"emergency":"stop"}));
	console.log("Arrêt d'urgence envoyé au module" + ws.url);
}

function emergencyStop(){
	// De la page réglage
	sendStop(websocket);
}

/**
 * Function sending emergency stop to all module
 *
 * Opens a websocket for every module
 */
function stopAll(){
	// Send all stop signals
	let websockets = [];
	for (const [key, group] of Object.entries(groupsDict)){
	    console.log(group);
		for (let module of group.modules){
			if (url === module.url)
				sendStop(websocket);
			else
				websockets.push( stopWS(module.url));
		}
	}
	// Set timeout to keep socket alive after the current function end, so the sockets have time to open and send the
	// stop signal.
	setTimeout(function() {
	  for (let ws of websockets){
			console.log(ws);
			ws.close();
		}
    }, 15000);
}

/**
 * Prints the current script/library version to the custom console
 */
function printLibraryVersion(){
	window.addEventListener('load', function () {
		let $console = document.getElementById("console");
		let $span = document.createElement("p");
		$span.innerText = versionLib;
		$span.style.color = "#bbd8ff";
		$console.appendChild($span)
	})
}

/**
 * Prints the msg to the window's custom console
 * @param msg - The message
 *
*/
function print(msg) {
	let log = "Log:" + msg + '   |   timestamp' + Date.now();
	let $console = document.getElementById("console");
    let $span = document.createElement("p");
	$span.innerText = log;
	$console.appendChild($span)
}

/**
 * Log errors in the handmade console on the page
 *
 * @param msg(string) - The message
 * @param source
 * @param lineNo
 * @param columnNo
 * @param error
 */
function onError(msg, source, lineNo, columnNo, error) {
	let log = "Error:" + msg + ' \n|---> ' + source+ " " + lineNo +"\n\n";
	let $console = document.getElementById("console");
    let $span = document.createElement("p");
	$span.innerText = log;
	$console.appendChild($span)
}

/**
 * Function that shows/hides the console
 */
function toogleConsole(){
	let $console = document.getElementById('console');
	let isVisible = $console.style.display !== "none";
	console.log(isVisible)
	console.log($console.style.display);

	if (isVisible)
		$console.style.display = "None";
	else
		$console.style.display = "block";

}

